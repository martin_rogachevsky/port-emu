package application;

import application.controllers.*;
import infrastructure.*;
import infrastructure.interfaces.IView;
import presentation.*;
import presentation.views.*;

import java.util.Stack;


/**
 * Main class of the application.
 */
public class Port {
    private static Stack<String> path;
    private static InputHandler handler;
    private static Viewer viewer;

    /**
     * Configuration of application components.
     * @throws Exception Can be caused by opening DAO connections.
     */
    private static void configure() throws Exception{
        //path configuration
        path = new Stack<>();
        path.push("lib");
        path.push("menu");

        //InputHandler configuration
        handler = new InputHandler();

        //Viewer configuration
        viewer = new Viewer();
        viewer.addView("err", new ErrorView());

        //Dao configuration
    }

    /**
     * Start point of the application.
     * @param args Arguments from console.
     */
    public static void main(String[] args) {
        try{
            configure();
        }catch (Exception e){
            System.out.println("Configuration error!");
            return;
        }

        boolean stop = true;
        String input;
        Response response;
        IView view = viewer.getView(path.peek(),null);

        while (stop){
            input =  view.show();
            if (input.equalsIgnoreCase("quit")) stop = false;
            else{
                input = path.peek() + ";" + input;
                try{
                    response = handler.handleInput(input);
                    view = viewer.getView(response, path);
                }
                catch (Exception e){
                    viewer.getView("err", e).show();
                }
            }
        }
    }
}
