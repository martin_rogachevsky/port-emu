package presentation;

import infrastructure.interfaces.IView;
import infrastructure.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Viewer class.
 * Stores map of views and names of views.
 * Provides view depends on {@code Response} or directly name of the view.
 */
public class Viewer {
    private Map<String, IView> views;

    public Viewer(){
        views = new HashMap<>();
    }

    /**
     * Method provides view depends on {@param response} and {@param path}.
     * @return - view that supposed to be shown.
     * @throws Exception can be caused by asking for non-existing view.
     */
    public IView getView(Response response, Stack<String> path) throws Exception{
        IView view = null;
        switch (response.code){
            case Ok:
                view = getView(path.peek(), response.param);
                break;
            case Fail:
                throw new Exception((String) response.param);
            case Redir:
                path.push(response.link);
                view = getView(response.link, response.param);
                if (view == null)
                    throw new Exception("There no view for "+response.param+" path!");
                break;
            case Back:
                path.pop();
                view = getView(path.peek(), response.param);
                break;
        }
        return view;
    }

    /**
     * Method provides view depends on {@param response} and {@param path}.
     * @return - view that supposed to be shown.
     */
    public IView getView(String name, Object param) {
        IView view = views.getOrDefault(name,null);
        view.clean();
        view.build(param);
        return view;
    }

    /**
     * Method adds view {@param view} with name {@param name}.
     */
    public void addView(String name, IView view){
        views.put(name, view);
    }

}
