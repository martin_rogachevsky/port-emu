package presentation.views;

import infrastructure.interfaces.IView;

/**
 * Error view class.
 */
public final class ErrorView implements IView{
    private String message;

    /**
     * Method shows view and retrieves user input.
     * @return - user input.
     */
    @Override
    public String show() {
        System.out.println("..................................................");
        System.out.print("ERROR : ");
        System.out.println(message);
        System.out.println("..................................................");
        return null;
    }

    /**
     * Method that allows view to get data from outside.
     * @param param - data that used by view.
     */
    @Override
    public void build(Object param) {
        if (param instanceof Exception)
            message = ((Exception)param).getMessage();
    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {
        message = null;
    }
}
