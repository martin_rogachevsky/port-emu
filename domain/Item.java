package domain;

/**
 * Abstract class that represents domain item.
 */
public abstract class Item {
    /**
     * Identifier for accessing the item.
     */
    public int id;
}
