package infrastructure.interfaces;

import infrastructure.Response;

/**
 * Interface for controller.
 * Controller should get command from a InputHandler and provide a set of methods to handle all kind of commands.
 * There necessarily should be method for coming back to the previous view.
 */
public interface IController {

    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     */
    Response handleCommand(String command);

    /**
     * Method to handle 'come-back' type of command.
      * @return is the response that means coming back.
     */
    Response back();
}
