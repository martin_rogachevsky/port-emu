package infrastructure;

/**
 * Enum determines available response codes.
 */
public enum ResponseCode {
    /**
     * Action finished successfully.
     */
    Ok,

    /**
     * Action failed.
     */
    Fail,

    /**
     * Action is a redirection.
     */
    Redir,

    /**
     * Action as a redirection to the previous view.
     */
    Back
}
